package main;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.base.CaseFormat;


public class Creator {
	private Properties config;
	private Map<String, String> mapping;
	private Logger logger;
	private static final Integer COPY_LIMIT = 2;
	private static final String PACKAGE_XML_PATH = "package/package.xml";
	private static final String MAPPING_FILE_PATH = "mapping/mapping.txt";
	private static final String LOG_DIR = "logs/%s.txt";
	
	@SuppressWarnings("serial")
	private static final Map<String, String> SPECIAL_CONTAINER = new HashMap<String, String>(){{
		put("CustomLabel", "CustomLabels"); 
	}};
	
	private static final String METADATA_NS = "http://soap.sforce.com/2006/04/metadata";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Creator c = new Creator();
	}
	
	public Creator(){
		this.getLogger();
		try{
			this.getProperties();
			this.readMapping();
			this.process();
		}catch(Exception e){
			logger.log(Level.SEVERE, e.getMessage(), e);
			JOptionPane.showMessageDialog(null, "Error. Check the logs for details.");
		}
	}
	
	private void getLogger(){
		logger = Logger.getLogger("packagecreator");  
	    FileHandler fh;  

	    try {  

	        // This block configure the logger with handler and formatter
	    	java.util.Date date= new java.util.Date();
	    	String timestamp = new SimpleDateFormat("MMddyyyyHHmmss").format(new Timestamp(date.getTime()));
	        fh = new FileHandler(String.format(LOG_DIR, "log-" + timestamp));  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);

	    } catch (SecurityException e) {
	        e.printStackTrace();
	        JOptionPane.showMessageDialog(null, "ERROR in creating log." + e.getMessage());
	    } catch (IOException e) {  
	        e.printStackTrace();
	        JOptionPane.showMessageDialog(null, "ERROR in creating log." + e.getMessage());
	    }
	}
	
	private void getProperties(){
		config = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("config.properties");

			// load a properties file
			config.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
			logger.log(Level.SEVERE, ex.getMessage(), ex);
			JOptionPane.showMessageDialog(null, "Error. Check the logs for details.");
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
					JOptionPane.showMessageDialog(null, "Error. Check the logs for details.");
					e.printStackTrace();
				}
			}
		}
	}

	private void readMapping(){
		BufferedReader br = null;
		mapping = new HashMap<String, String>();
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(MAPPING_FILE_PATH));

			while ((sCurrentLine = br.readLine()) != null) {
				String[] parts = sCurrentLine.split(",\\s*");
				mapping.put(parts[0].trim(), parts[1].trim());
			}

		} catch (IOException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, e.getMessage(), e);
			JOptionPane.showMessageDialog(null, "Error. Check the logs for details.");
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
				logger.log(Level.SEVERE, ex.getMessage(), ex);
				JOptionPane.showMessageDialog(null, "Error. Check the logs for details.");
			}
		}

	}
	
	// Empties a directory
	private void clearDirectory(File directory){
		File[] files = directory.listFiles();
		if(files!=null) { //some JVMs return null for empty dirs
	        for(File f: files) {
	        	deleteFile(f);
	        }
	    }
	}
	
	// Deletes a file or a directory
	private  void deleteFile(File file) {
	    File[] files = file.listFiles();
	    if(files!=null) { //some JVMs return null for empty dirs
	        for(File f: files) {
	            if(f.isDirectory()) {
	            	deleteFile(f);
	            } else {
	                f.delete();
	            }
	        }
	    }
	    file.delete();
	}

	// Get all files in a directory and subdirectory
	private List<String> getFileNames(List<String> fileNames, Path dir) {
	    try(DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
	        for (Path path : stream) {
	            if(path.toFile().isDirectory()) {
	                getFileNames(fileNames, path);
	            } else {
	                fileNames.add(path.toAbsolutePath().toString());
	            }
	        }
	    } catch(IOException e) {
	    	logger.log(Level.SEVERE, e.getMessage(), e);
			JOptionPane.showMessageDialog(null, "Error. Check the logs for details.");
	        e.printStackTrace();
	    }
	    return fileNames;
	}
	
	// The retrieval process
	private void process(){
		// Read package.xml
		String typeName = "";
		String componentName = "";
		boolean hasWarning = false;
		try {
			File fXmlFile = new File(PACKAGE_XML_PATH);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			// Clear output directory
			File output = new File(config.getProperty("dest"));
			if(output.exists()){
				clearDirectory(output);
			}
			
			Map<String, Map<String, Set<Node>>> complexMetadata = new HashMap<String, Map<String, Set<Node>>>();
			NodeList nList = doc.getElementsByTagName("types");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				Element eElement = (Element) nNode;
				typeName = eElement.getElementsByTagName("name").item(0).getTextContent();
				NodeList members = eElement.getElementsByTagName("members");
				
				Path src = Paths.get(config.getProperty("src"), mapping.get(typeName));
				Path dest = Paths.get(config.getProperty("dest"), mapping.get(typeName));
				
				List<String> srcFiles = new ArrayList<String>();
				getFileNames(srcFiles, src);
				
				for (int i = 0; i < members.getLength(); i++) {
					componentName = members.item(i).getTextContent().trim();
					String[] componentNameParts = componentName.split("\\.");
					if(componentNameParts.length > 1 || SPECIAL_CONTAINER.containsKey(typeName)){
						String container = "";
						String childComponent = "";
						
						if(SPECIAL_CONTAINER.containsKey(typeName)){
							container = SPECIAL_CONTAINER.get(typeName);
							childComponent = componentName;
						}else{
							container = componentNameParts[0];
							childComponent = componentNameParts[1];
						}
						
						if(!complexMetadata.containsKey(typeName)){
							complexMetadata.put(typeName, new HashMap<String, Set<Node>>());
						}
						
						if(!complexMetadata.get(typeName).containsKey(container)){
							complexMetadata.get(typeName).put(container, new HashSet<Node>());
						}
						
						Path metaFilePath = null;
						for(String filePath: srcFiles){
							Path file = Paths.get(filePath);
					    	String fName = file.getFileName().toString();
							if(fName.startsWith(container + ".")){
								metaFilePath = file;
								
								// Create file to destination
								File destFile = new File(Paths.get(dest.toString(), fName).toString());
								if(!destFile.exists()){
									dest.toFile().mkdirs();
									Document reference = dBuilder.parse(metaFilePath.toFile());
									Document destDoc = dBuilder.newDocument();
									Element rootElement = destDoc.createElement(reference.getDocumentElement().getTagName());
									rootElement.setAttribute("xmlns", METADATA_NS);
									destDoc.appendChild(rootElement);
									
									TransformerFactory transformerFactory = TransformerFactory.newInstance();
									Transformer transformer = transformerFactory.newTransformer();
									DOMSource source = new DOMSource(destDoc);
									StreamResult result = new StreamResult(destFile);
									transformer.transform(source, result);
								}
								break;
							}
						}
						
						Document containerObj = dBuilder.parse(metaFilePath.toFile());
						containerObj.getDocumentElement().normalize();
						Element root = containerObj.getDocumentElement();
						NodeList children = root.getChildNodes();
						boolean found = false;
						for(int n = 0; n < children.getLength(); n++ ){
							Node childNode = children.item(n);
							if(childNode.getNodeType() == Node.ELEMENT_NODE){
								Element child = (Element)childNode;
								
								if(child.getElementsByTagName("fullName").getLength() == 0){
									continue;
								}
								
								String fullName = child.getElementsByTagName("fullName").item(0).getTextContent();
								if(fullName.equals(childComponent) && validateNode(child.getNodeName(), typeName)){
									found = true;
									complexMetadata.get(typeName).get(container).add(child.cloneNode(true));
								}
							}
						}
						
						if(!found){
							logger.log(Level.WARNING, container + " "+ childComponent + " not found.");
							hasWarning = true;
						}
						
					}else{
						Integer filesCopied = 0;
						
						String[] componentPath = componentName.split("/");
						String baseName = componentPath[0];
						
						// For components with directories
						if(componentPath.length > 1){
							baseName = componentPath[componentPath.length - 1];
						}
						
						boolean found = false;
						for(String filePath: srcFiles){
							Path srcFile = Paths.get(filePath);
					    	String fName = srcFile.getFileName().toString();
					    	Path relativePath = src.relativize(srcFile);
					    	Path destFile = Paths.get(dest.toString(), relativePath.toString());
					    	if(fName.startsWith(baseName + ".")){
					    		found = true;
					    		try{
					    			destFile.toFile().getParentFile().mkdirs();
					    			Files.copy(srcFile, destFile, StandardCopyOption.REPLACE_EXISTING);
					    		}catch(Exception e){
					    			logger.log(Level.SEVERE, e.getMessage(), e);
					    			JOptionPane.showMessageDialog(null, "Error. Check the logs for details.");
					    			e.printStackTrace();
					    			continue;
					    		}
								
								filesCopied++;
								if(filesCopied == COPY_LIMIT){
									break;
								}
					    	}
						}
						if(!found){
							logger.log(Level.WARNING, componentName + " not found.");
							hasWarning = true;
						}
					}
				}
			}
			
			for(String type: complexMetadata.keySet()){
				for(String container: complexMetadata.get(type).keySet()){
					Path dest = Paths.get(config.getProperty("dest"), mapping.get(type));
					List<String> destFiles = new ArrayList<String>();
					getFileNames(destFiles, dest);
					for(String filePath: destFiles){
						Path fp = Paths.get(filePath);
						String fName = fp.getFileName().toString();
						if(fName.startsWith(container + ".")){
							String destPath = Paths.get(dest.toString(), fName).toString();
							Document destDoc = dBuilder.parse(destPath);
							Element rootElement = destDoc.getDocumentElement();
							rootElement.normalize();
							
							for(Node child: complexMetadata.get(type).get(container)){
								String fullName = ((Element)child).getElementsByTagName("fullName").item(0).getTextContent();
								String nodeName = child.getNodeName();
								
								boolean alreadyExists = false;
								NodeList members = rootElement.getElementsByTagName(nodeName);
								for(int i = 0; i < members.getLength(); i++){
									String memberName = ((Element)members.item(i)).getElementsByTagName("fullName").item(0).getTextContent();
									if(memberName.equals(fullName)){
										alreadyExists = true;
									}
								}
								
								if(!alreadyExists){
									rootElement.appendChild(destDoc.importNode(child, true));
								}
							}
							
							TransformerFactory transformerFactory = TransformerFactory.newInstance();
							Transformer transformer = transformerFactory.newTransformer();
							transformer.setOutputProperty(OutputKeys.INDENT, "yes");
							DOMSource source = new DOMSource(destDoc);
							StreamResult result = new StreamResult(new File(destPath));
							transformer.transform(source, result);
						}
					}
				}
			}
			
			Files.copy(Paths.get(PACKAGE_XML_PATH), Paths.get(config.getProperty("dest"), "package.xml"), StandardCopyOption.REPLACE_EXISTING);
			logger.info("Package successfully created.");

			if(!hasWarning){
				JOptionPane.showMessageDialog(null, "Package creation successful!");
			}else{
				JOptionPane.showMessageDialog(null, "Package creation successful with some warnings. Check your logs.");
			}
			
		} catch (Exception e) {
	    	e.printStackTrace();
	    	logger.log(Level.SEVERE, "Error on " + componentName + " ("+ typeName +").", e);
			JOptionPane.showMessageDialog(null, "Error. Check the logs for details.");
	    }
		
	}

	// Check if we're looking at correct node. We're having a trial and error here since
	// we have no way to check (yet) what the correct node name would be
	private boolean validateNode(String nodeName, String typeName) {
		boolean result = false;
		String snakeVersion = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_HYPHEN, typeName);
		String lowerCamel = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, typeName);
		String[] splits = snakeVersion.split("-");
		String secondWord = splits[splits.length-1];
		
		// This down here is the trial and error part
		if(splits.length > 2){
			String firstWordRemove = String.join("-", new ArrayList(Arrays.asList(Arrays.copyOfRange(splits, 1, splits.length))));
			secondWord = CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, firstWordRemove);
		}
		
		String lowerCamelPlural = pluralize(lowerCamel);
		String secondWordPlural = pluralize(secondWord);
		
		if(nodeName.equals(lowerCamelPlural) || nodeName.equals(secondWordPlural) || nodeName.equals(lowerCamel)){
			result = true;
		}
		return result;
	}

	// Simple pluralizer
	private String pluralize(String word) {
		String plural = "";
		if(word.charAt(word.length()-1) == 's' || word.charAt(word.length()-1) == 'x'){
			plural = word + "es";
		}else{
			plural = word + "s";
		}
		return plural;
	}
	
	
}
