How to use:
1. Configuration:
   - Edit config.properties file.
     - src - the directory where the package.xml of your project resides
     - dest - the directory where the you want to create the retrieved package

2. Create your package.xml.
   - Put it inside the 'package' folder

3. Double click pkgCre8.
   - This should retrieve the components from your package.xml from the src directory
   - Logs can be found in the 'logs' folder
   - Retrieve package can be found in your specified 'dest' from the config.properties file